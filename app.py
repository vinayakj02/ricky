from flask import Flask
import os

app = Flask(__name__)

@app.route("/")
def hello():
    return "dQw4w9WgXcQ"


if __name__ == "__main__":
    port = int(os.environ.get("PORT", 42069))
    app.run(debug=True,host='0.0.0.0',port=port)
